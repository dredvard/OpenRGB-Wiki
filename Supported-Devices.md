![image](uploads/bf30bed71f782d01334b73caab75a61a/image.png)

The device categories listed below have OpenRGB support and the specific devices listed have been tested to work with OpenRGB.  If your specific device is not listed, that does not necessarily mean it won't work with OpenRGB, just that it hasn't been tested before.  If you have a device that works with OpenRGB and isn't listed below, please let me know!  I will add it to the list.

If you have a project or product that is compatible with OpenRGB, you may use the above OpenRGB Compatible badge on your project/product page.

### Motherboard RGB Systems

* [ASUS Aura (SMBus Variants)](ASUS-Aura-Overview)
    * ASUS PRIME X370-Pro
    * ASUS PRIME X470-Pro
    * ASUS PRIME X399-A
    * ASUS PRIME B450M-Gaming
    * ASUS PRIME Z270-A
    * ASUS PRIME Z370-A
    * ASUS ROG Crosshair VI Hero
    * ASUS ROG STRIX X399-E Gaming
    * ASUS ROG Strix B350-F Gaming
    * ASUS ROG Strix B450-F Gaming
    * ASUS ROG Strix Z270-E
    * ASUS ROG Strix Z370-E
    * ASUS ROG Strix Z490-E Gaming
    * ASUS TUF B450 Plus Gaming
* [ASUS Aura (USB Variants)](ASUS Aura USB)
    * [ASUS Aura Addressable Headers](ASUS-Aura-Addressable-Header)
    * ASUS X570 Motherboards
* [Gigabyte Aorus RGB Fusion 1.0](Gigabyte-RGB-Fusion-1.0)
    * Gigabyte Aorus X370 Gaming 5
* Gigabyte Aorus RGB Fusion 2.0 (SMBus)
* Gigabyte Aorus RGB Fusion 2.0 (USB)
    * Gigabyte X570 Aorus Extreme
    * Gigabyte X570 Aorus Master
    * Gigabyte X570 Aorus Pro
    * Gigabyte X570 Gaming X
    * Gigabyte X570 I Aorus Pro Wifi
    * Gigabyte TRX40 Aorus Master
    * Gigabyte Z390 Aorus Ultra
* [ASRock Polychrome RGB](ASRock-Polychrome-RGB)
    * ASRock B450 Steel Legend
    * ASRock B450M Steel Legend
    * ASRock Fatal1ty B350 Gaming-ITX/ac
    * ASRock B450M/ac
    * ASRock X570 Taichi
* [MSI-RGB](MSI-RGB)

### RGB RAM Modules

* [ASUS Aura Based](ASUS-Aura-Overview)
    * G.Skill Trident Z RGB
    * G.Skill Trident Z Neo
    * G.Skill Trident Z Royal
    * Geil Super Luce
    * Team T-Force Delta RGB
    * OLOy WarHawk RGB
    * ADATA SPECTRIX RGB
    * Thermaltake TOUGHRAM RGB
* [Corsair Vengeance RGB](Corsair-Vengeance-RGB)
* [Corsair Vengeance Pro RGB](Corsair-Vengeance-Pro-RGB)
* [HyperX RGB Memory](HyperX-Predator-RGB)
    * HyperX Predator RGB
    * HyperX Fury RGB
* [Patriot Viper RGB](Patriot-Viper-RGB)
* [Crucial Ballistix RGB](Crucial-Ballistix-RGB)

### Graphics Cards

* [ASUS Aura GPUs](Asus-Aura-GPU)
    * ASUS ROG Strix RX580
    * ASUS ROG Strix GTX1080Ti
* [EVGA GPUs](EVGA-GPU)
    * EVGA GeForce GTX 1070 FTW
* [Galax GPUs](Galax-GPUs)
    * Galax KFA2 RTX2070
* [Gigabyte Aorus RGB Fusion GPUs](Gigabyte-RGB-Fusion-GPU)
    * Gigabyte Aorus GTX1080Ti Xtreme Waterforce WB
    * Gigabyte RTX2070 SUPER GAMING 
* [MSI GPUs](MSI-GPU)
    * MSI GeForce RTX 2060 Super Gaming X
    * MSI GeForce RTX 2070 Super Gaming X Trio
    * MSI GeForce RTX 2080 Gaming X Trio
    * MSI GeForce RTX 2080 Super Gaming X Trio
    * MSI GeForce RTX 2080Ti Gaming X Trio
    * MSI GeForce RTX 2060 Gaming Z 6G
    * MSI GeForce RTX 2060 Super ARMOR OC
    * MSI GeForce RTX 2070 ARMOR
    * MSI GeForce RTX 2080Ti Sea Hawk EK X
* [Sapphire Nitro Glow V1](Sapphire-Nitro-Glow-V1)
    * Sapphire RX580 Nitro+
* [Sapphire Nitro Glow V2](Sapphire-Nitro-Glow-V2)
* [Sapphire Nitro Glow V3](Sapphire-Nitro-Glow-V3)

### LED Strip and Fan Controllers

* [ASUS ROG Aura Terminal](ASUS-Aura-Addressable-Header)
* [NZXT Hue+](NZXT-Hue-Plus)
* [NZXT Hue 2 Devices](NZXT-Hue-2)
    * NZXT Hue 2
    * NZXT Hue 2 Ambient
    * NZXT Smart Device V2
    * NZXT RGB & Fan Controller
* [Corsair Lighting Node Devices](Corsair-Lighting-Node-Devices)
    * Corsair Lighting Node Core
    * Corsair Lighting Node Pro
    * Corsair Commander Pro
    * Corsair LS100 Lighting Kit
    * Corsair 1000D Obsidian
    * Corsair SPEC OMEGA RGB
    * Corsair LT100
    * [Corsair Lighting Protocol (Arduino)](https://github.com/Legion2/CorsairLightingProtocol)
* [Keyboard Visualizer Arduino LED strips](Keyboard-Visualizer-LED-Strips)
* [E1.31 Streaming ACN Protocol](E1.31)
    * [ESPixelStick](https://github.com/forkineye/ESPixelStick)
    * [WLED](https://github.com/Aircoookie/WLED)
* [Thermaltake Riing Plus](Thermaltake-Riing)

### Lights

* Philips Hue (WIP, philips_hue_development branch)
* Philips Wiz
* Espurna (https://github.com/xoseperez/espurna)

### Fans and Coolers

* [AMD Wraith Prism](AMD-Wraith-Prism)
* [Corsair Hydro Series](Corsair-Hydro-Series)
  * Corsair H100i PRO RGB
* [NZXT Kraken Xx2](NZXT-Kraken)
* [NZXT Kraken Xx3](NZXT-Hue-2)

### Keyboards

* [ASUS ROG Aura Core Laptops](ASUS-Aura-Core)
    * ASUS ROG Zephyrus M GM501GM
* [Corsair RGB Keyboards](Corsair-Peripheral-Protocol)
    * Corsair K65 RGB
    * Corsair K65 Lux RGB
    * Corsair K65 RGB Rapidfire
    * Corsair K68 RGB
    * Corsair K70 RGB
    * Corsair K70 Lux RGB
    * Corsair K70 RGB Rapidfire
    * Corsair K70 RGB MK.2
    * Corsair K70 RGB MK.2 SE
    * Corsair K70 RGB MK.2 Low Profile
    * Corsair K95 RGB
    * Corsair K95 RGB Platinum
    * Corsair Strafe
    * Corsair Strafe MK.2
* [Ducky RGB Keyboards](Ducky-Keyboards)
    * Ducky Shine 7
    * Ducky One 2
    * Ducky One 2 TKL
* [HyperX RGB Keyboards](HyperX-Alloy-Elite)
    * HyperX Alloy Elite
    * HyperX Alloy Origins
* [Logitech RGB Keyboards](Logitech-Keyboards)
    * Logitech G213
    * Logitech G512
    * Logitech G610
    * Logitech G810 Orion Spectrum
* [MSI Steelseries 3-Zone Keyboard](MSI-3-Zone-Keyboard)
    * MSI GS63VR
* [Redragon Keyboards (and compatibles)](Redragon-K556-Devarajas)
    * Redragon K550 Yama
    * Redragon K552 Kumara
    * Redragon K556 Devarajas
    * Tecware Phantom Elite
    * Warrior Kane TC235
* SteelSeries Keyboards
    * SteelSeries Apex 5
    * SteelSeries Apex 7
    * SteelSeries Apex 7 TKL
    * SteelSeries Apex Pro
    * SteelSeries Apex Pro TKL
* [TTEsports Poseidon Z RGB](Thermaltake-Poseidon-Z-RGB)

### Mice

* ASUS Mice
    * ROG Gladius II
    * ROG Gladius II Core
    * ROG Gladius II Origin
* [Corsair Mice](Corsair-Peripheral-Protocol)
    * Corsair M65 RGB PRO
    * Corsair M65 RGB Elite
* Glorious Model O
* HyperX Mice
    * HyperX Pulsefire Surge
* [Logitech Mice](Logitech-G203)
    * Logitech G203 Prodigy
    * Logitech G203 Lightsync
    * Logitech G403 Prodigy
    * Logitech G403 Hero
    * Logitech G502 Proteus Spectrum
    * Logitech G502 Hero
    * Logitech G Lightspeed Wireless Gaming Mouse
    * Logitech G Pro Wireless Gaming Mouse (Wired)
    * Logitech G Powerplay Mousepad with Lightspeed"
* [Redragon Mice](Redragon-M711-Cobra)
    * Redragon M711 Cobra
    * Redragon M715 Dagger
* Roccat Mice
    * Roccat Kone Aimo
* SteelSeries Mice
    * SteelSeries Rival 100
    * SteelSeries Rival 100 DotA 2 Edition
    * SteelSeries Rival 105
    * SteelSeries Rival 110
    * SteelSeries Rival 300
    * Acer Predator Gaming Mouse (Rival 300)
    * SteelSeries Rival 300 CS:GO Fade Edition
    * SteelSeries Rival 300 CS:GO Fade Edition (stm32)
    * SteelSeries Rival 300 CS:GO Hyperbeast Edition
    * SteelSeries Rival 300 DotA 2 Edition
    * SteelSeries Rival 300 HP Omen Edition
    * SteelSeries Rival 300 Black Ops Edition

### Mousemats

* [Cooler Master MP750](Cooler-Master-MP750)
* Corsair MM800 Polaris
* HyperX Fury Ultra

### Game Controlers

* [Sony DualShock 4](Sony-DualShock-4)

### Other

* Corsair ST100 Headset Stand

### Other projects integrated

* [OpenRazer](https://github.com/openrazer/openrazer) / [OpenRazer-Win32](https://github.com/CalcProgrammer1/openrazer-win32)
    * Keyboards
        * Razer BlackWidow 2019
        * Razer BlackWidow Chroma
        * Razer BlackWidow Chroma Overwatch
        * Razer BlackWidow Chroma Tournament Edition
        * Razer BlackWidow Chroma V2
        * Razer BlackWidow Elite
        * Razer BlackWidow X Chroma
        * Razer BlackWidow X Tournament Edition Chroma
        * Razer Cynosa Chroma
        * Razer DeathStalker Chroma
        * Razer Ornata Chroma
        * Razer Huntsman
        * Razer Huntsman Elite
        * Razer Huntsman Tournament Edition
    * Mice
        * Razer Abyssus Elite DVa Edition
        * Razer Abyssus Essential
        * Razer Basilisk
        * Razer DeathAdder Chroma
        * Razer DeathAdder Elite
        * Razer Diamondback Chroma
        * Razer Lancehead Tournament Edition
        * Razer Mamba 2012
        * Razer Mamba Chroma
        * Razer Mamba Elite
        * Razer Mamba Tournament Edition
        * Razer Naga Chroma
        * Razer Naga Epic Chroma (*)
        * Razer Naga Hex V2
        * Razer Naga Trinity
        * Razer Viper Ultimate
    * Laptops
        * Razer Blade Stealth
        * Razer Blade Stealth (Late 2016)
        * Razer Blade Stealth (Mid 2017)
        * Razer Blade Stealth (Late 2017)
        * Razer Blade Stealth (2019)
        * Razer Blade Stealth (Late 2019)
        * Razer Blade (Late 2016)
        * Razer Blade (QHD)
        * Razer Blade 15 (2018)
        * Razer Blade 15 (2018) Mercury
        * Razer Blade 15 (2018) Base Model
        * Razer Blade 15 (2019) Advanced
        * Razer Blade 15 (Mid 2019) Mercury
        * Razer Blade 15 (Mid 2019) Base Model
        * Razer Blade 15 Studio Edition (2019)
        * Razer Blade Pro (Late 2016)
        * Razer Blade Pro (2017)
        * Razer Blade Pro FullHD (2017)
        * Razer Blade Pro 17 (2019)
        * Razer Blade Pro (Late 2019)
    * Headsets
        * Razer Kraken 7.1 Chroma
        * Razer Kraken V2 Chroma
        * Razer Tiamat 7.1 V2 (*)
    * Mousemats
        * Razer Firefly
        * Razer Goliathus Chroma
        * Razer Goliathus Extended Chroma
    * Speakers
        * Razer Nommo Chroma
        * Razer Nommo Pro
    * Accessories
        * Razer Base Station Chroma
        * Razer Chroma HDK
        * Razer Core
        * Razer Mug Holder Chroma

(*) - Device not supported in upstream OpenRazer and requires a custom build.

* Faustus (ASUS TUF Laptop Keyboards) (Linux)

# Disabled Devices

The following devices have support in OpenRGB but this support is currently disabled due to unimplemented detection or bricking risk.

### Motherboard RGB Systems

* [MSI Mystic Light](MSI-Mystic-Light)

### RGB RAM Modules

* [Gigabyte Aorus RGB RAM (Partial support)](Gigabyte-RGB-Fusion-2.0-DRAM)